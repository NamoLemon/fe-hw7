/*
3.Реализовать функцию фильтра массива по указанному типу данных.
Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. 
Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных. 
Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент, 
за исключением тех, тип которых был передан вторым аргументом. То есть, если передать массив ['hello', 'world', 23, '23', null], 
и вторым аргументом передать 'string', то функция вернет массив [23, null].
 */


	function filterBy(arr, type){
		const newArr = arr.filter(function(i){
			if (typeof(i) !== type){
				return i;
			}
		})
		return newArr;
	}


const arr = [56, 'lalalend', null, 1, 77, true, 'que'];

console.log(filterBy(arr, "number"));
